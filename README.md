# Very Hungry Penguins [![build status](https://gitlab.com/softwa/very-hungry-penguins/badges/master/build.svg)](https://gitlab.com/softwa/very-hungry-penguins/commits/master)

Hungry Penguins game written in [OCaml]. Done as part of the M1 Jacques Herbrand (MPRI) formation at ENS Paris-Saclay.

## Get started

### Dependencies

You need `ocaml`, `opam`, `m4` and `libgtk2.0-dev` on your system. For Debian and derivatives see `scripts/ci_before_script.sh`. Then:

```sh
eval $(opam config env)
opam install -y alcotest yojson lablgtk ocamlfind ocamlbuild
```

### Build

```SOME CHANGES to make ```

### Install

```make install```

### Try it

Launch the server:

```sh
./server.native
```

Launch the client:

```sh
./client.native -a $(hostname)
```

### Generate the documentation
```make doc```
then
```xdg-open doc/index.html```

### Run tests
```make test```

### Generate a debian package
```make deb-pkg```

## Contributing

See [CONTRIBUTING].

[OCaml]: https://ocaml.org/
[CONTRIBUTING]: https://gitlab.com/softwa/very-hungry-penguins/blob/master/CONTRIBUTING.md
